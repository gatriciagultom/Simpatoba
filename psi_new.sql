-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2018 at 11:06 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `psi_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `email`, `password`, `role`, `username`, `alamat`, `jenis_kelamin`, `nama`, `no_telp`, `status`, `tanggal_lahir`) VALUES
(1, 'riris@mail.com', '1234567890', 'Donatur', 'riris', NULL, NULL, NULL, NULL, b'1111111111111111111111111111111', NULL),
(2, 'win@mail.com', 'winner1997', 'Donatur', 'winner', NULL, NULL, NULL, NULL, b'1111111111111111111111111111111', NULL),
(3, 'win@mail.com', 'sukarelawan', 'Sukarelawan', 'sukarelawan', 'jl.surga neraka', 'Laki-laki', 'winner', '085261579157', b'1111111111111111111111111111111', '07/06/1997'),
(4, 'winner@mail.com', 'sukarelawan', 'Sukarelawan', 'sukarelawan', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `date_created` varchar(255) DEFAULT NULL,
  `id_admin` int(11) NOT NULL,
  `isi_artikel` longtext,
  `judul_artikel` varchar(255) DEFAULT NULL,
  `nama_admin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donasi`
--

CREATE TABLE `donasi` (
  `id` int(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `id_donatur` int(20) NOT NULL,
  `jenis_donasi` varchar(50) NOT NULL,
  `jumlah_barang` int(20) DEFAULT NULL,
  `jumlah_uang` int(20) DEFAULT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `no_telp` int(20) NOT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donasi_barang`
--

CREATE TABLE `donasi_barang` (
  `id` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `id_donatur` int(11) NOT NULL,
  `jumlah_barang` varchar(255) DEFAULT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `no_telp` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tanggal_penjemputan` varchar(255) NOT NULL,
  `id_penerima` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donasi_barang`
--

INSERT INTO `donasi_barang` (`id`, `alamat`, `id_donatur`, `jumlah_barang`, `nama_barang`, `nama_lengkap`, `no_telp`, `status`, `tanggal_penjemputan`, `id_penerima`) VALUES
(1, 'jl.aan', 2, '100', 'baju lebaran', 'winner siringoringo', '0862187581', 'Menunggu Konfirmasi Sukarelawan', '2018-06-13 11:17:44', NULL),
(2, 'jl.Perwira Bukit Barisan', 2, '100', 'Makanan Sahur', 'Winner Immanuel', '085261579157', 'Menunggu Konfirmasi Sukarelawan', '2018-06-13 11:23:55', NULL),
(3, 'dasf', 2, '90', 'makan', 'D', '12881', 'Diterima', '2018-06-13 12:39:50', NULL),
(4, '', 2, '0', '', 'Wi', '', 'Menunggu Konfirmasi Sukarelawan', '2018-06-13 12:49:27', NULL),
(5, 'jhgjahj', 2, 'jashjdha', 'jhasjhja', 'aajskja', 'jhjshjahsjdh', 'Diterima', '2018-06-13 12:49:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `donasi_uang`
--

CREATE TABLE `donasi_uang` (
  `id` int(11) NOT NULL,
  `jumlah_uang` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `tanggal_kirim` varchar(255) DEFAULT NULL,
  `bukti` varchar(255) DEFAULT NULL,
  `id_donatur` int(11) DEFAULT NULL,
  `id_penerima` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donasi_uang`
--

INSERT INTO `donasi_uang` (`id`, `jumlah_uang`, `nama_lengkap`, `no_telp`, `status`, `tanggal_kirim`, `bukti`, `id_donatur`, `id_penerima`) VALUES
(1, '0', '', '', 'Menunggu Konfirmasi Sukarelawan', '2018-06-13 15:45:58', NULL, 2, NULL),
(2, '800000000', 'JokoWidodo', '085261579157', 'Diterima', '2018-06-13 15:45:58', 'contoh.jpg', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keuangan`
--

CREATE TABLE `keuangan` (
  `id` int(11) NOT NULL,
  `id_admin` int(20) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `nama_admin` varchar(255) DEFAULT NULL,
  `nama_sponsor` varchar(100) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `tanggal_terima` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

CREATE TABLE `model` (
  `id` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `nama_lengkap` varchar(25) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donasi`
--
ALTER TABLE `donasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donasi_barang`
--
ALTER TABLE `donasi_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donasi_uang`
--
ALTER TABLE `donasi_uang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keuangan`
--
ALTER TABLE `keuangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donasi`
--
ALTER TABLE `donasi`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donasi_barang`
--
ALTER TABLE `donasi_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `donasi_uang`
--
ALTER TABLE `donasi_uang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `keuangan`
--
ALTER TABLE `keuangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `model`
--
ALTER TABLE `model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `donasi`
--
ALTER TABLE `donasi`
  ADD CONSTRAINT `donasi_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
