package org.psi.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.psi.models.Akun;
import org.psi.models.DonasiBarang;
import org.psi.models.DonasiUang;
import org.psi.repository.DonasiBarangRepository;
import org.psi.repository.DonasiUangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class DonasiController {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date date = new Date();
	
	@Autowired
	private DonasiBarangRepository dbrepo;
	@Autowired
	private DonasiUangRepository durepo;
	
	@RequestMapping(value = "/donasibarangd" , method = RequestMethod.POST)
	public String barang(Model model ,HttpServletRequest req, DonasiBarang db,
			@RequestParam("namalengkap") String nama,
			@RequestParam("alamat") String alamat,
			@RequestParam ("notelp") String notelp,
			@RequestParam ("namabarang") String namabarang,
			@RequestParam("jumlahbarang") String jumlah){
		Akun akun =  (Akun)req.getSession().getAttribute("akunLogin");
		db.setAlamat(alamat);
		db.setJumlahbarang(jumlah);
		db.setNotelp(notelp);
		db.setIddonatur(akun.getId());
		db.setNamabarang(namabarang);
		db.setNamalengkap(nama);
		db.setStatus("Menunggu Konfirmasi Sukarelawan");
		db.setTanggal(dateFormat.format(date));
		dbrepo.save(db);
		return "redirect:/psi";
	}
	@RequestMapping(value = "/donasiuangd" , method = RequestMethod.POST)
	public String uang(Model model,HttpServletRequest req ,@Valid DonasiUang du,
			@RequestParam("foto") MultipartFile file,
			@RequestParam ("namalengkap") String nama,
			@RequestParam ("notelp") String notelp,
			@RequestParam ("jumlahuang") String jumlah){
		Akun akun = (Akun)req.getSession().getAttribute("akunLogin");
		if(!file.isEmpty()){
			du.setBukti(file.getOriginalFilename());
			String name = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File("C:/xampp/htdocs/simpatoba/PSI4/src/main/resources/static/gambar"+name)));
				stream.write(bytes);
				stream.close();
				System.out.println(stream);
			} catch (Exception e) {
				return "Gagal Upload Bukti" +name +"=>"+e.getMessage();
			}
		}
		du.setIddonatur(akun.getId());
		du.setJumlahuang(jumlah);
		du.setNotelp(notelp);
		du.setStatus("Menunggu Konfirmasi Sukarelawan");
		du.setTanggalkirim(dateFormat.format(date));	
		du.setNamalengkap(nama);
		durepo.save(du);
		return "redirect:/psi";
	}
	
	@RequestMapping(value="konfirmasibarang/{id}")
	public String konfirmasibarang(@PathVariable int id,HttpServletRequest req){
		DonasiBarang db = dbrepo.findOne(id);
		db.setStatus("Diterima");
		dbrepo.save(db);
		return "redirect:/psi";
	}
	
	@RequestMapping(value="konfirmasiuang/{id}")
	public String konfirmasiuang(@PathVariable int id,HttpServletRequest req){
		DonasiUang du = durepo.findOne(id);
		du.setStatus("Diterima");
		durepo.save(du);
		return "redirect:/psi";
	}
	
}