package org.psi.controllers;

import javax.validation.Valid;

import org.psi.dao.KeuanganDao;
import org.psi.models.Keuangan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
public class HomeAdminController {
	private KeuanganDao kDao;

	@Autowired
	public void settDao(KeuanganDao kDao) {
		this.kDao = kDao;
	}

	@RequestMapping(value = "/createKeuangan", method = RequestMethod.POST)
	public String createKeuangan(Model model, @Valid Keuangan keuangan,  BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return "redirect:/createKeuangan";
		}
		model.addAttribute("keuangan", kDao.saveOrUpdate(keuangan));
		return "redirect:/createKeuangan";
	}
	
}
