package org.psi.controllers;

import javax.validation.Valid;

import org.psi.dao.DonasiDao;

import org.psi.models.Donasi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeDonaturController {
	private DonasiDao tDao;
	
	@Autowired
	public void settDao(DonasiDao tDao) {
		this.tDao = tDao;
	}
	@RequestMapping(value = "/createDonasi", method = RequestMethod.POST)
	public String createDonasi(Model model, @Valid Donasi donasi, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return "redirect:/psi/donatur/createDonasi";
		}
		
		model.addAttribute("donasi", tDao.saveOrUpdate(donasi));
		return "redirect:/psi/donatur/createDonasi";
	}	
	
	
}
 