package org.psi.dao;

import java.util.List;

import org.psi.models.Akun;

public interface AkunDao {
	List<Akun> getAllAkun();
	Akun saveOrUpdate(Akun akun);
	Akun login(String username, String password );

}
