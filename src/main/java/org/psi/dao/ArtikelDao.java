package org.psi.dao;
import java.util.List;

import org.psi.models.Artikel;

public interface ArtikelDao {
	List<Artikel> getAllArtikel();
	List<Artikel> getAllArtikelByIdAdmin(int id);
	Artikel saveOrUpdate(Artikel artikel);
	Artikel findArtikelById(int id);
	void deleteArtikelById(int id);
}
