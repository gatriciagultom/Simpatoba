package org.psi.dao;

import java.util.List;

import org.psi.models.Donasi;

public interface DonasiDao {
	List<Donasi> getAllDonasi();
	List<Donasi> getAllDonasiByIdDonatur(int id);
	List<Donasi>getAllDonasiByStatus(String status);
	Donasi saveOrUpdate(Donasi donasi);
	Donasi findDonasiById(int id);
	void deleteDonasiById(int id);
	void updateStatusDonasiById(int id);
	void konfirmasi(int id);
}
