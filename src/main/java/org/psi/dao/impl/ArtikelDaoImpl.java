package org.psi.dao.impl;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.psi.dao.ArtikelDao;
import org.psi.models.Artikel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArtikelDaoImpl implements ArtikelDao {

	private EntityManagerFactory emf;
	
	@Override
	public List<Artikel> getAllArtikel() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Artikel", Artikel.class).getResultList();
	}
	@Override
	public Artikel saveOrUpdate(Artikel artikel) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		//		Artikel saved = new Artikel();
		//		dptkan tanggal
		//		saved.setDate_created(tanggal yg didapat);
		Artikel saved = em.merge(artikel);
		em.getTransaction().commit();
		em.close();
		return saved;
	}
	@Override
	public Artikel findArtikelById(int id) {
		EntityManager em = emf.createEntityManager();		
		return em.find(Artikel.class, id);
	}
	@Override
	public List<Artikel> getAllArtikelByIdAdmin(int id) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Artikel where id_admin="+id, Artikel.class).getResultList();	
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	@Override
	public void deleteArtikelById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Artikel.class, id));
		System.out.println(findArtikelById(id).getId());
		em.getTransaction().commit();
	}
}