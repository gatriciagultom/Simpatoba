package org.psi.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.psi.dao.KeuanganDao;
import org.psi.models.Keuangan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class KeuanganDaoImpl implements KeuanganDao {

	private EntityManagerFactory emf;

	
	@Override
	public List<Keuangan> getAllKeuangan() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Keuangan", Keuangan.class).getResultList();
	}

	@Override
	public Keuangan saveOrUpdate(Keuangan keuangan) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Keuangan saved = em.merge(keuangan);
		em.getTransaction().commit();
		return saved;
	}
	@Override
	public List<Keuangan> getAllKeuanganByStatus(String status) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Keuangan where status='"+status+"'", Keuangan.class).getResultList();
	}
	@Override
	public void updateStatusKeuanganById(int id) {
		Keuangan Keuangan = findKeuanganById(id);
		Keuangan.setStatus("Done");
		Keuangan = saveOrUpdate(Keuangan);		
	}
	@Override
	public Keuangan findKeuanganById(int id) {
		EntityManager em = emf.createEntityManager();		
		return em.find(Keuangan.class, id);
	}
	@Override
	public List<Keuangan> getAllKeuanganByIdAdmin(int id) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Keuangan where id_admin="+id, Keuangan.class).getResultList();
	}
	public EntityManagerFactory getEmf() {
		return emf;
	}
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public void deleteKeuanganById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Keuangan.class, id));
		System.out.println(findKeuanganById(id).getId());
		em.getTransaction().commit();
	}

	


}
