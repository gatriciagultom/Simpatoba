package org.psi.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Akun {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull(message="username tidak boleh kosog")
	@Size(min=5, max = 20, message="username harus 5 - 20 karakter")
	private String username;
	
	@NotNull(message="password tidak boleh kosong")
	@Size(min=5, max = 20, message="Passwod min. 8 karakter")
	private String password;
	
	private String role;
	
	@NotNull(message="email tidak boeh kosong")
	private String email;
	
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

 
	public Akun() {
		super();
		// TODO Auto-generated constructor stub
	}
	  
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	

}
