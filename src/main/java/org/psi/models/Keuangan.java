package org.psi.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
@Entity
public class Keuangan {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getId_admin() {
			return id_admin;
		}
		public void setId_admin(int id_admin) {
			this.id_admin = id_admin;
		}
		public String getNama_sponsor() {
			return nama_sponsor;
		}
		public void setNama_sponsor(String nama_sponsor) {
			this.nama_sponsor = nama_sponsor;
		}
		public String getJumlah() {
			return jumlah;
		}
		public void setJumlah(String jumlah) {
			this.jumlah = jumlah;
		}
		public String getTanggal_terima() {
			return tanggal_terima;
		}
		public void setTanggal_terima(String tanggal_terima) {
			this.tanggal_terima = tanggal_terima;
		}
		
		
		public String getNama_admin() {
			return nama_admin;
		}
		public void setNama_admin(String nama_admin) {
			this.nama_admin = nama_admin;
		}
	
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		private int id_admin;
		private String nama_admin;
		@NotNull(message="Nama Sponsor Tidak Boleh Kosong")
		private String nama_sponsor;
		@NotNull(message="Jumlah Tidak Boleh Kosong")
		private String jumlah;
		@NotNull(message="Tanggal Tidak Boleh Kosong")
		private String tanggal_terima;
		
		private String status;
		
}
