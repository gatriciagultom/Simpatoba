package org.psi.repository;
import java.util.List;

import org.psi.models.DonasiBarang;
import org.springframework.data.jpa.repository.JpaRepository;
public interface DonasiBarangRepository extends JpaRepository <DonasiBarang , Integer>{
	List<DonasiBarang> findByiddonatur(int id);
}
